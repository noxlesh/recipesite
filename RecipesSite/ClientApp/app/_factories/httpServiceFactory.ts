import { XHRBackend } from "@angular/http";
import { BaseRequestOptions} from "@angular/http";
import {HttpService} from "../services/http.service";
import {JWTRequestOptions} from "../_helpers/request-options";

function httpServiceFactory(backend: XHRBackend, options: BaseRequestOptions) {
    return new HttpService(backend, <JWTRequestOptions>options)
} 

export { httpServiceFactory };