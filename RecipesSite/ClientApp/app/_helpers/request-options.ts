import { BaseRequestOptions } from "@angular/http";

export class JWTRequestOptions extends BaseRequestOptions {
    public token: string|null;
    
    constructor (customOptions?: any){
        super();
        this.token = localStorage.getItem('token');
        console.log("Token in storage: " + this.token);
        if (this.headers) {
            this.headers.append('Content-Type', 'application/json');
            this.headers.append('Authorization', 'Bearer ' + this.token);
        }
    }
}