import {Component, Inject, OnInit} from '@angular/core';
import { Unit } from "../../models/unit";
import { UnitsService} from "../../services/units.service";

@Component({
    selector: 'units',
    templateUrl: './units.component.html',
    providers: [UnitsService]
})
export class UnitsComponent implements OnInit {

    constructor(private svc: UnitsService){}
    units: Unit[] = [];
    unit: Unit = new Unit("");
    table: boolean = true;
    addForm: boolean = false;
    editForm: boolean = false;

    ngOnInit() {
        this.loadUnits()
    }

    private loadUnits() {
        this.svc.getUnits()
            .subscribe(data =>  {
                if (data.status == 200){
                    console.log("Units loaded")
                    this.units = JSON.parse(data.text());
                } else {
                    console.log("No units loaded")
                }
            });
    }

    add() {
        this.cancel();
        this.table = false;
        this.addForm = true;
    }

    save() {
        if (this.unit.id == null) {
            this.svc.createUnit(this.unit)
                .subscribe(data => this.loadUnits());
        } else {
            this.svc.updateUnit(this.unit)
                .subscribe(data => this.loadUnits());
        }
        this.cancel();
        
    }
    
    editUnit(u: Unit) {
        this.unit = u;
        this.table = false;
        this.editForm = true;
    }
    
    cancel() {
        this.unit = new Unit("");
        this.table = true;
        this.addForm = false;
        this.editForm = false;
    }
    
    delete(u: Unit) {
        if (u.id) {
            this.svc.deleteUnit(u.id)
                .subscribe(data => this.loadUnits());
        }
    }
}