import {Component, Inject, OnInit} from '@angular/core';
import { Recipe } from "../../models/recipe";
import { Product} from "../../models/product";
import { Unit } from "../../models/unit";
import {Validators, FormGroup, FormArray, FormBuilder, AbstractControl} from '@angular/forms';
import { RecipesService } from "../../services/recipes.service";
import {UnitsService} from "../../services/units.service";
import {ProductsService} from "../../services/products.service";
import {IngredientsService} from "../../services/ingredients.service";
import {Ingredient} from "../../models/ingredient";
import {ProductInterface, RecipeInterface} from "./recipe.interface";

@Component({
    selector: 'reipes',
    templateUrl: './recipes.component.html',
    providers: [RecipesService, UnitsService, ProductsService, IngredientsService]
})
export class RecipesComponent implements OnInit {
    public recipeForm: FormGroup;
    public products: ProductInterface[] = [];
    public recipeList = true;
    public recipeAdd = false;
    
    constructor(private fb: FormBuilder,
                private  psvc: ProductsService, 
                private  usvc: UnitsService,
                private  rsvc: RecipesService,
                private  isvc: IngredientsService,
                ){}
                
    add() {
        this.recipeList = false;
        this.recipeAdd = true;
    }
    
    cancel() {
        this.recipeAdd = false;
        this.recipeList = true;
    }
    
    ngOnInit(): void {
        this.getProducts();
        this.recipeForm = this.fb.group({
            name: ['', [Validators.required, Validators.minLength(5)]],
            description: ['', [Validators.required, Validators.minLength(10)]],
            ingredients: this.fb.array([
                this.initIngredient(),
            ])
        });
    }

    private initIngredient() {
        return this.fb.group({
            product: [''],
            count: ['']
        });
    }
    
    private addIngredient() {
        const control = <FormArray>this.recipeForm.controls['ingredients'];
        control.push(this.initIngredient());
    }
    
    private removeIngredient(i: number) {
        const control = <FormArray>this.recipeForm.controls['ingredients'];
        control.removeAt(i);
    }
    
    
    save(fg: AbstractControl): void {
        let name = fg.get('name')!.value;
        let desc = fg.get('description')!.value;
        let recipe = new Recipe(name, desc, 0);
        this.rsvc.createRecipe(recipe).subscribe(rdata =>{
            if (rdata.status == 200){
                console.log("Created recipe: " +rdata.text());
               recipe = JSON.parse(rdata.text());
                let fa = <FormArray>fg.get('ingredients');
                for ( let ingredientCtrl of fa.controls){
                    let product = <ProductInterface>ingredientCtrl.get('product')!.value;
                    let count = ingredientCtrl.get('count')!.value;
                    let ingredient = new Ingredient(product.id, recipe.id || 0, count);
                    this.isvc.createIngredient(ingredient).subscribe(idata => {
                       if (idata.status == 200) {
                           console.log("Created ingredient: " + idata.text());
                       } else {
                           console.log("Ingredient not created");
                       }
                    });
                }
            } else {
                console.log("Recipe didnt created!")
            }
        });
       
        
    }
    
    getProducts(){
        let products: Product[] = [];
        this.psvc.getProducts()
            .subscribe(data =>  {
                if (data.status == 200){
                    console.log("Products loaded")
                    products = JSON.parse(data.text());
                    for ( let p of products){
                        let unit: Unit = new Unit("");
                        this.usvc.getUnit(p.unitId || 0).subscribe(udata => {
                            if (udata.status == 200){
                                console.log("Unit loaded");
                                unit = JSON.parse(udata.text());
                                let obj = {id: p.id || 0, name: p.name, unit: unit.name};
                                console.log("Pmodel: " + JSON.stringify(obj));
                                this.products.push(obj);
                            }
                                
                            else 
                                console.log("Unit not loaded");
                        });
                        
                        
                    }
                } else {
                    console.log("No products loaded")
                }
            });
    }
}