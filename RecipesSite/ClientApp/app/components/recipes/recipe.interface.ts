export interface RecipeInterface {
    name: string;
    description: string;
    ingredients: IngredientInterface[];
}

export interface IngredientInterface {
    product: ProductInterface;
    count: number;
}

export interface ProductInterface {
    name: string;
    unit: string;
    id: number;
}