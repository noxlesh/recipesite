import {Component, OnInit} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import {User} from "../../models/user";
import { HttpService } from "../../services/http.service";

@Component({
    selector: 'register',
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
    
    constructor(private http: HttpService) {}

    private user: User;
    
    ngOnInit() {
        this.user = new User("", "");
    }

    onSubmit() {
        console.log(JSON.stringify(this.user))
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        this.http.post("account/register", JSON.stringify(this.user), options)
            .subscribe(resp => {
                if (resp.status == 200){
                    console.log("JWT response: " + resp.text());
                    localStorage.setItem('token', resp.text());
                } else {
                    console.log("JWT response status 500!");
                }
                
            });
    }
}

