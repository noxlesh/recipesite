import { Component, OnInit } from '@angular/core';
import { Unit } from "../../models/unit";
import { UnitsService } from "../../services/units.service";
import { Product } from "../../models/product";
import { ProductsService } from "../../services/products.service";
import {letProto} from "rxjs/operator/let";

@Component({
    selector: 'products',
    templateUrl: './products.component.html',
    providers: [UnitsService, ProductsService]
})
export class ProductsComponent implements OnInit {

    constructor(private usvc: UnitsService, private psvc: ProductsService){}
     
    units: Unit[] = [];
    product: Product = new Product("");
    products: Product[] = [];
    table: boolean = true;
    addForm: boolean = false;
    editForm: boolean = false;
    
    private getUnitName (id: number): string {
        let name =  "undefined";
        for (let unit of this.units) {
            if (unit.id == id)
                return unit.name
        }
        return name;
    }
    
    ngOnInit() {
        this.loadProducts()
    }

    private loadProducts() {
        this.usvc.getUnits()
            .subscribe(data =>  {
                if (data.status == 200){
                    console.log("Units loaded")
                    this.units = JSON.parse(data.text());
                } else {
                    console.log("No units loaded")
                }
            });
        
        this.psvc.getProducts()
            .subscribe(data =>  {
                if (data.status == 200){
                    console.log("Products loaded")
                    this.products = JSON.parse(data.text());
                } else {
                    console.log("No products loaded")
                }
            });
    }

    add() {
        this.cancel();
        this.table = false;
        this.addForm = true;
    }

    save() {
        if (this.product.id == null) {
            this.psvc.createProduct(this.product)
                .subscribe(data => this.loadProducts());
        } else {
            this.psvc.updateProduct(this.product)
                .subscribe(data => this.loadProducts());
        }
        this.cancel();

    }

    editProduct(p: Product) {
        this.product = p;
        this.table = false;
        this.editForm = true;
    }

    cancel() {
        this.product = new Product("");
        this.table = true;
        this.addForm = false;
        this.editForm = false;
    }

    delete(p: Product) {
        if (p.id) {
            this.psvc.deleteProduct(p.id)
                .subscribe(data => this.loadProducts());
        }
    }
}