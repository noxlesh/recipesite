import { Injectable } from "@angular/core";
import { HttpService} from "./http.service";
import { Unit } from "../models/unit";
import { Product} from "../models/product";

@Injectable()
export class ProductsService {
    private url = "products";
    
    constructor(private http: HttpService) {
    }

    getProducts() {
        return this.http.get(this.url);
    }

    createProduct(product: Product) {
        return this.http.post(this.url, product);
    }

    updateProduct(product: Product) {
        return this.http.put(this.url + '/' + product.id, product);
    }

    deleteProduct(id: number) {
        return this.http.delete(this.url + '/' + id);
    }
}
