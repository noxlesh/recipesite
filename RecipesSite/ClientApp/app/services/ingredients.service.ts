import { Injectable } from "@angular/core";
import { HttpService} from "./http.service";
import { Ingredient } from "../models/ingredient";

@Injectable()
export class IngredientsService {
    private url = "ingredients";

    constructor(private http: HttpService) {
    }

    getIngredient() {
        return this.http.get(this.url);
    }

    createIngredient(ingredient: Ingredient) {
        return this.http.post(this.url, ingredient);
    }

    updateIngredient(ingredient: Ingredient) {
        return this.http.put(this.url + '/' + ingredient.id, ingredient);
    }

    deleteIngredient(id: number) {
        return this.http.delete(this.url + '/' + id);
    }
}
