import { Injectable } from "@angular/core";
import { HttpService} from "./http.service";
import { Unit } from "../models/unit";

@Injectable()
export class UnitsService {
    private url = "units";
    
    constructor(private http: HttpService) {
    }

    getUnits() {
        return this.http.get(this.url);
    }
    
    getUnit(id: number) {
        return this.http.get(this.url + '/' + id);
    }

    createUnit(unit: Unit) {
        return this.http.post(this.url, unit);
    }

    updateUnit(unit: Unit) {
        return this.http.put(this.url + '/' + unit.id, unit);
    }

    deleteUnit(id: number) {
        return this.http.delete(this.url + '/' + id);
    }
}
