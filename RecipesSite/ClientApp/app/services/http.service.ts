import {Inject, Injectable} from "@angular/core";
import { Http, RequestOptionsArgs, Headers, XHRBackend } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { JWTRequestOptions } from "../_helpers/request-options";

@Injectable () 
export class HttpService extends Http {
    public token: string;
    apiUrl = 'http://localhost:5000/api/';
    //@Inject('BASE_URL') private apiUrl: string;
    
    constructor(backend: XHRBackend, defaultOptions: JWTRequestOptions) {
        super(backend, defaultOptions);
    }
    
    get(url: string, options?: RequestOptionsArgs): Observable<any> {
        return super.get(this.getFullUrl(url), this.requestOptions(options));
    }
    
    post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        console.log("POST: " + JSON.stringify(body));
        return super.post(this.getFullUrl(url), body, this.requestOptions(options));
    }
    
    put(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        return super.put(this.getFullUrl(url), body,  this.requestOptions(options));
    }
    
    delete(url: string, options?: RequestOptionsArgs) {
        return super.delete(this.getFullUrl(url), this.requestOptions(options));
    }
        
    private getFullUrl(url: string): string {
        return this.apiUrl + url;
    }
    
    private requestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new JWTRequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        return options;
    }
}