import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";
import { Recipe } from "../models/recipe";

@Injectable()
export class RecipesService {

    private url = "recipes";

    constructor(private http: HttpService) {
    }

    getRecipes() {
        return this.http.get(this.url);
    }

    createRecipe(recipe: Recipe) {
        return this.http.post(this.url, recipe);
    }
    
    updateRecipe(recipe: Recipe) {
        return this.http.put(this.url + '/' + recipe.id, recipe);
    }
    
    deleteRecipe(id: number) {
        return this.http.delete(this.url + '/' + id);
    }
}