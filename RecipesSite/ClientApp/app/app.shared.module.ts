import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from "./components/register/register.component";
import { LoginComponent } from "./components/login/login.component";
import { UnitsComponent} from "./components/units/units.component";
import { RecipesComponent } from "./components/recipes/recipes.component";

import { httpServiceFactory } from "./_factories/httpServiceFactory";
import { HttpService } from "./services/http.service";
import {Product} from "./models/product";
import {ProductsComponent} from "./components/products/products.component";

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        RegisterComponent,
        LoginComponent,
        RecipesComponent,
        UnitsComponent,
        ProductsComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'nav-menu', component: NavMenuComponent },
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },
            { path: 'recipes', component: RecipesComponent },
            { path: 'units', component: UnitsComponent },
            { path: 'products', component: ProductsComponent},
            { path: '**', redirectTo: 'home' }
        ])
    ],
    providers: [
        {
            provide: HttpService,
            useFactory: httpServiceFactory,
            deps: [XHRBackend, RequestOptions]
        }
    ]
})

export class AppModuleShared {
}
