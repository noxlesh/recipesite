export class Ingredient {
    constructor(
        public productId: number,
        public recipeId: number,
        public count: number,
        public id?: number
    ) {}
}