import { Unit } from "./unit";

export class Product {
    constructor(
        public name: string,
        public unitId?: number,
        public id?: number
    ) {}
}