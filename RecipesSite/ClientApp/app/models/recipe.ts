export class Recipe {
    constructor(
        public name: string,
        public description: string,
        public userId: number,
        public id?: number
    ) {}
}