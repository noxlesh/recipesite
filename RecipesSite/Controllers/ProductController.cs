﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RecipesSite.EF;
using RecipesSite.EF.Entities;

namespace RecipesSite.Controllers
{
    [Route("api/products")]
    public class ProductController : Controller
    {
        private RecipesDbContext _ctx;

        public ProductController(RecipesDbContext ctx)
        {
            _ctx = ctx;
        }
        
        [HttpGet]
        public JsonResult Get()
        {
            var r = new JsonResult(_ctx.Products);
            if (!_ctx.Products.Any())
                r.StatusCode = 204;
            return r;
        }

        [HttpGet("{id}")]
        public Product Get(int id)
        {
            Product product = _ctx.Products.FirstOrDefault(x => x.Id == id);
            return product;
        }
         
        [HttpPost]
        public IActionResult Post([FromBody]Product product)
        {
            if(ModelState.IsValid)
            {
                _ctx.Products.Add(product);
                _ctx.SaveChanges();
                return Ok(product);
            }
            return BadRequest(ModelState);
        }
         
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Product product)
        {
            if (ModelState.IsValid)
            {
                _ctx.Update(product);
                _ctx.SaveChanges();
                return Ok(product);
            }
            return BadRequest(ModelState);
        }
         
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Product product = _ctx.Products.FirstOrDefault(x => x.Id == id);
            if(product!=null)
            {
                _ctx.Products.Remove(product);
                _ctx.SaveChanges();
            }
            return Ok(product);
        }
    }
}