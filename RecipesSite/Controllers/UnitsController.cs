﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RecipesSite.EF;
using RecipesSite.EF.Entities;

namespace RecipesSite.Controllers
{
    [Route("api/units")]
    public class UnitsController : Controller
    {
        private RecipesDbContext _ctx;

        public UnitsController(RecipesDbContext ctx)
        {
            _ctx = ctx;
        }
        
        //[Authorize]
        [HttpGet]
        public JsonResult Get()
        {
            var r = new JsonResult(_ctx.Units);
            if (!_ctx.Units.Any())
                r.StatusCode = 204;
            return r;
        }

        [HttpGet("{id}")]
        public Unit Get(int id)
        {
            Unit unit= _ctx.Units.FirstOrDefault(x => x.Id == id);
            return unit;
        }
         
        [HttpPost]
        public IActionResult Post([FromBody]Unit unit)
        {
            if(ModelState.IsValid)
            {
                _ctx.Units.Add(unit);
                _ctx.SaveChanges();
                return Ok(unit);
            }
            return BadRequest(ModelState);
        }
         
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Unit unit)
        {
            if (ModelState.IsValid)
            {
                _ctx.Update(unit);
                _ctx.SaveChanges();
                return Ok(unit);
            }
            return BadRequest(ModelState);
        }
         
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Unit unit = _ctx.Units.FirstOrDefault(x => x.Id == id);
            if(unit!=null)
            {
                _ctx.Units.Remove(unit);
                _ctx.SaveChanges();
            }
            return Ok(unit);
        }
    }
}