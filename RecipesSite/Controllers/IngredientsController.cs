﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using RecipesSite.EF;
using RecipesSite.EF.Entities;

namespace RecipesSite.Controllers
{
    [Route("api/ingredients")]
    public class IngredientsController : Controller
    {
        private RecipesDbContext _ctx;

        public IngredientsController(RecipesDbContext ctx)
        {
            _ctx = ctx;
        }
        
        [HttpGet]
        public JsonResult Get()
        {
            var r = new JsonResult(_ctx.Ingredients);
            if (!_ctx.Ingredients.Any())
                r.StatusCode = 204;
            return r;
        }

        [HttpGet("{id}")]
        public Ingredient Get(int id)
        {
            Ingredient ingredient = _ctx.Ingredients.FirstOrDefault(x => x.Id == id);
            return ingredient;
        }
         
        [HttpPost]
        public IActionResult Post([FromBody]Ingredient ingredient)
        {
            if(ModelState.IsValid)
            {
                _ctx.Ingredients.Add(ingredient);
                _ctx.SaveChanges();
                return Ok(ingredient);
            }
            return BadRequest(ModelState);
        }
         
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Ingredient ingredient)
        {
            if (ModelState.IsValid)
            {
                _ctx.Update(ingredient);
                _ctx.SaveChanges();
                return Ok(ingredient);
            }
            return BadRequest(ModelState);
        }
         
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Ingredient ingredient = _ctx.Ingredients.FirstOrDefault(x => x.Id == id);
            if(ingredient!=null)
            {
                _ctx.Ingredients.Remove(ingredient);
                _ctx.SaveChanges();
            }
            return Ok(ingredient);
        }
    }
}