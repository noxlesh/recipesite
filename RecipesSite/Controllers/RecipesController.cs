﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using RecipesSite.EF;
using RecipesSite.EF.Entities;

namespace RecipesSite.Controllers
{
    [Route("api/recipes")]
    public class RecipesController : Controller
    {
        private RecipesDbContext _ctx;

        public RecipesController(RecipesDbContext ctx)
        {
            _ctx = ctx;
        }
        
        [HttpGet]
        public JsonResult Get()
        {
            var r = new JsonResult(_ctx.Recipes);
            if (!_ctx.Recipes.Any())
                r.StatusCode = 204;
            return r;
        }

        [HttpGet("{id}")]
        public Recipe Get(int id)
        {
            Recipe recipe = _ctx.Recipes.FirstOrDefault(x => x.Id == id);
            return recipe;
        }
         
        [HttpPost]
        public IActionResult Post([FromBody]Recipe recipe)
        {
            if(ModelState.IsValid)
            {
                _ctx.Recipes.Add(recipe);
                _ctx.SaveChanges();
                return Ok(recipe);
            }
            return BadRequest(ModelState);
        }
         
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Recipe recipe)
        {
            if (ModelState.IsValid)
            {
                _ctx.Update(recipe);
                _ctx.SaveChanges();
                return Ok(recipe);
            }
            return BadRequest(ModelState);
        }
         
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Recipe recipe = _ctx.Recipes.FirstOrDefault(x => x.Id == id);
            if(recipe!=null)
            {
                _ctx.Recipes.Remove(recipe);
                _ctx.SaveChanges();
            }
            return Ok(recipe);
        }
    }
}