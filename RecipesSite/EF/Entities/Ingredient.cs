﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RecipesSite.EF.Entities
{
    [Table("ingredients")]
    public class Ingredient
    {
        public int Id { get; set; }
        [Required]
        public int RecipeId { get; set; }
        public Recipe Recipe { get; set; }
        [Required]
        public int ProductId { get; set; } 
        public Product Product {get; set; }
        [Required]
        public float Count { get; set; }
    }
}