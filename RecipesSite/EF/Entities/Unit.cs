﻿//using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RecipesSite.EF.Entities
{
    [Table("unit")]
    public class Unit
    {
        public int Id { get; set; }
        [Required,MaxLength(10)]
        public string Name { get; set; }
        
        public virtual ICollection<Product> Products { get; set; }
    }
}