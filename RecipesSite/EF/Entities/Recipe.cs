﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RecipesSite.EF.Entities
{
    [Table("recipe")]
    public class Recipe
    {
        public int Id { get; set; }
        [Required, MaxLength(30)]
        public string Name { get; set; }
        [Required, MaxLength(1000)]
        public string Description { get; set; }
        
        public virtual ICollection<Ingredient> Ingredients { get; set; }
        public virtual User User { get; set; }
    }
}