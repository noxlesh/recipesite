﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RecipesSite.EF.Entities
{
    [Table("product")]
    public class Product
    {
        public int Id { get; set; }
        [Required,MaxLength(30)]
        public string Name { get; set; }
        [Required]
        public int UnitId { get; set; }
        public Unit Unit { get; set; }
        
    }
}