﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace RecipesSite.EF.Entities
{
    public class User : IdentityUser
    {
        public virtual ICollection<Recipe> Recipes { get; set; }
    }
}